/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.reto3;

/**
 *
 * @author betox
 */
public class Cuenta {
    // balance
    private double balance;
    // numero de cuenta
    private int numero;
    
    public Cuenta(int a) {
        balance = 0.0;
        numero = a;
    }
    
    public void depositar(double valor) {
        if (valor > 0)
            balance += valor;
        else
            System.err.println("No se puede depositar una cantidad negativa.");
    }
    
    public void retirar(double valor) {
        if (valor > 0)
            balance -= valor;
        else
            System.err.println("No se puede retirar una cantidad negativo.");
    }
    
    public double getBalance() {
        return balance;
    }
    public int getNumero() {
        return numero;
    }
    
    public String toString() {
        return "Cuenta " + numero + ": balance = " + balance;
    }
    
    public final void print() {
        System.out.println(toString());
    }
}
